﻿'Requires installation of 'Microsoft-WindowsAPICodePack-Core' through NuGet.
'Requires installation of 'Microsoft-WindowsAPICodePack-Shell' through NuGet.
'Project:           FileSystemControl
'Author:            Nico Lalangan
'Date Created:      7/3/19
'Last Updated:      7/3/19

Imports System
Imports System.Windows.Forms
Imports Microsoft.Win32
Imports Microsoft.WindowsAPICodePack.Dialogs

Public Class FileSystemControl
  Enum ListOf
    Files = 0
    Directories = 1
    Both = 2
  End Enum

  Private ListOfFiles As String()
  Private ListOfFilePaths As String()
  Private ListOfFilesAndDirectories As String()

  Private Path As String = Nothing

  Public Sub Test()
    MsgBox("asd")
  End Sub

  Public Sub SetPath(Optional Path As String = Nothing)
    If IsNothing(Path) Then
      Dim Dialog As New CommonOpenFileDialog
      Dialog.RestoreDirectory = True
      Dialog.InitialDirectory = "C:\\Users"
      Dialog.IsFolderPicker = True
      Dialog.Title = "Choose a folder"

      Dialog.ShowDialog()
      Me.Path = Dialog.FileName
    Else
      Me.Path = Path
    End If
  End Sub

  Public Function GetPath()
    Return Me.Path
  End Function

  Public Sub SetLIstOfFiles(Optional List As String() = Nothing)
    ListOfFiles = List
  End Sub

  Public Function GetFilePaths() As String()
    Return ListOfFilePaths
  End Function

  Public Function GetListofFiles(Optional Path As String = Nothing, Optional Filters As String() = Nothing, Optional Level As Int16 = 0) As String()
    If (Path <> Nothing) Then Me.Path = Path

    SetLIstOfFiles()

    Dim List As Object = IO.Directory.GetFiles(Me.Path)
    Dim Count As Int16 = 0

    For Each File As String In List
      File = IO.Path.GetFileName(File)

      If (File.ToUpper.Contains("THUMBS.DB")) Then
        'Do Nothing
      ElseIf (Not IsNothing(Filters)) Then
        For Each Filter As String In Filters
          If (File.ToUpper.Contains(Filter.ToUpper)) Then
            ReDim Preserve ListOfFilePaths(Count)
            ReDim Preserve ListOfFiles(Count)
            ListOfFilePaths(Count) = Path & File
            ListOfFiles(Count) = File
            Count += 1
            Exit For
          End If
        Next Filter
      Else
        ReDim Preserve ListOfFilePaths(Count)
        ReDim Preserve ListOfFiles(Count)
        ListOfFilePaths(Count) = Path & File
        ListOfFiles(Count) = File
        Count += 1
      End If
    Next File

    If (Level > 0) Then
      Dim DirList As Object = IO.Directory.GetDirectories(Me.Path)
      For Each Dir As String In DirList
        Count = GetListofFilesInSubLevel(Dir & "\", Filters, Level - 1, Count)
      Next Dir
    End If

    Return ListOfFiles
  End Function

  Private Function GetListofFilesInSubLevel(Optional Path As String = Nothing, Optional Filters As String() = Nothing, Optional Level As Int16 = 0, Optional Count As Int16 = 0) As Int16
    If (Path <> Nothing) Then Me.Path = Path

    Dim List As Object = IO.Directory.GetFiles(Me.Path)

    For Each File As String In List
      File = IO.Path.GetFileName(File)

      If (File.ToUpper.Contains("THUMBS.DB")) Then
        'Do Nothing
      ElseIf (Not IsNothing(Filters)) Then
        For Each Filter As String In Filters
          If (File.ToUpper.Contains(Filter.ToUpper)) Then
            ReDim Preserve ListOfFilePaths(Count)
            ReDim Preserve ListOfFiles(Count)
            ListOfFilePaths(Count) = Path & File
            ListOfFiles(Count) = File
            Count += 1
            Exit For
          End If
        Next Filter
      Else
        ReDim Preserve ListOfFilePaths(Count)
        ReDim Preserve ListOfFiles(Count)
        ListOfFilePaths(Count) = Path & File
        ListOfFiles(Count) = File
        Count += 1
      End If
    Next File

    If (Level > 0) Then
      Dim DirList As Object = IO.Directory.GetDirectories(Me.Path)
      For Each Dir As String In DirList
        Count = GetListofFilesInSubLevel(Dir & "\", Filters, Level - 1, Count)
      Next Dir
    End If

    Return Count
  End Function

  Public Function GetListOfDirectories(Optional Path As String = Nothing, Optional Filters As String() = Nothing) As String()
    If (Path <> Nothing) Then Me.Path = Path

    Dim ListOfDirectories As String()
    Dim List As Object = IO.Directory.GetDirectories(Me.Path)
    Dim Count As Int16 = 0

    For Each File As String In List
      File = IO.Path.GetFileName(File)

      If (File.ToUpper.Contains("THUMBS.DB")) Then
        'Do Nothing
      ElseIf (Not IsNothing(Filters)) Then
        For Each Filter As String In Filters
          If (File.ToUpper.Contains(Filter.ToUpper)) Then
            ReDim Preserve ListOfDirectories(Count)
            ListOfDirectories(Count) = File
            Count += 1
            Exit For
          End If
        Next Filter
      Else
        ReDim Preserve ListOfDirectories(Count)
        ListOfDirectories(Count) = File
        Count += 1
      End If
    Next File

    Return ListOfDirectories
  End Function

  Public Function GetListOfAllFiles(Optional Path As String = Nothing, Optional Filters As String = Nothing, Optional Level As Int16 = 0) As String()
    If (Path <> Nothing) Then Me.Path = Path

    Dim ListOfDirectories As String()
    Dim List As Object = IO.Directory.GetFiles(Me.Path)
    Dim Count As Int16 = 0

    For Each File As String In List
      File = IO.Path.GetFileName(File)

      If (File.ToUpper.Contains("THUMBS.DB")) Then
        'Do Nothing
      ElseIf (Not IsNothing(Filters)) Then
        For Each Filter As String In Filters
          If (File.ToUpper.Contains(Filter.ToUpper)) Then
            ReDim Preserve ListOfDirectories(Count)
            ListOfDirectories(Count) = File
            Count += 1
            Exit For
          End If
        Next Filter
      Else
        ReDim Preserve ListOfDirectories(Count)
        ListOfDirectories(Count) = File
        Count += 1
      End If
    Next File

    Return ListOfDirectories
  End Function

  Public Sub PopulateListBox(ByRef ListBox As ListBox, Items As Object())
    If IsNothing(Items) Then
      ListBox.IsEnabled = False
    Else
      For Each Item In Items
        ListBox.Items.Add(Item)
      Next
      ListBox.IsEnabled = True
    End If
  End Sub
End Class