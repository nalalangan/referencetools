﻿'Requires installation of 'Microsoft.Office.Interop.Excel through NuGet.
'Author:            Nico Lalangan
'Date Created:      7/3/19
'Last Updated:      7/3/19

Imports System.IO
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel

Public Class ExcelControl
  Private oXl As Excel.Application = Nothing
  Private oWB As Workbook = Nothing
  Private oSheet As Worksheet = Nothing
  Private oRng As Range = Nothing

  Private Enum XlFormat
    Tabs = 1
    Commas = 2
    Spaces = 3
    Semicolons = 4
    [Nothing] = 5
  End Enum

  'Excel App Parameters
  Private NewApp As Boolean = False                                           'True to open a new Excel Application regardless if there is an already running Excel Application or not
  Private XlVisible As Boolean = True                                         'True to have Workbooks open as visible within created Excel App
  Private DisplayAlerts As Boolean = True                                     'True to have Workbooks show alert messages within created Excel App

  'Workbook Parameters
  Private FilePath As String = Nothing                                        'Nuff said
  Private FileName As String = Nothing                                        'Nuff said
  Private UpdateLinks As Object = XlUpdateLinks.xlUpdateLinksUserSetting      'True if User want links on Workbooks to be updated automatically
  Private [ReadOnly] As Boolean = False                                       'True to open the workbook in read-only mode
  Private Format As Integer = XlFormat.Nothing                                'No idea
  Private Password As String = Nothing                                        'For opening protected workbooks
  Private WriteResPassword As String = Nothing                                'For opening write-reserved workbook
  Private IgnoreReadOnlyRecommended As Boolean = False                        'To or not to ignore        'read-only recommended message       '
  Private Origin As XlPlatform = XlPlatform.xlWindows                         'xlMacintosh, xlWindows, or xlMSDOS
  Private Delimiter As Char = Nothing                                         'If is a text file and needs delimitting
  Private Editable As Boolean = False                                         'Special ribbon for editing?
  Private Notify As Boolean = True                                            'To or not to ignore        'read/write error message       '
  Private Converter As Object = Nothing                                       'No idea
  Private AddToMru As Boolean = False                                         'To or not to add opened file to        'Recently Used       '
  Private Local As Boolean = False                                            'No idea
  Private CorruptLoad As XlCorruptLoad = XlCorruptLoad.xlNormalLoad           'Action when file is corrupted
  Private Visible As Boolean = True                                           'Visibility of workbook


  Public Sub SetExcelParameters(Optional NewApp = Nothing, Optional XlVisible = Nothing, Optional DisplayAlerts = Nothing)
    If Not IsNothing(NewApp) Then Me.NewApp = NewApp
    If Not IsNothing(XlVisible) Then Me.XlVisible = XlVisible
    If Not IsNothing(DisplayAlerts) Then Me.NewApp = DisplayAlerts
  End Sub

  'Set Excel parameters
  Public Sub SetWorkBookParameters(Optional Filepath = Nothing, Optional UpdateLinks = Nothing, Optional [ReadOnly] = Nothing,
                           Optional Format = Nothing, Optional Password = Nothing, Optional WriteResPassword = Nothing,
                           Optional IgnoreReadOnlyRecommended = Nothing, Optional Origin = Nothing,
                           Optional Delimiter = Nothing, Optional Editable = Nothing, Optional Notify = Nothing,
                           Optional Converter = Nothing, Optional AddToMru = Nothing, Optional Local = Nothing,
                           Optional CorruptLoad = Nothing, Optional Visible = Nothing)

    If Not IsNothing(Filepath) Then Me.FilePath = Filepath
    If Not IsNothing(Filepath) Then Me.FileName = Path.GetFileName(Me.FilePath)
    If Not IsNothing(UpdateLinks) Then Me.UpdateLinks = UpdateLinks
    If Not IsNothing([ReadOnly]) Then Me.[ReadOnly] = [ReadOnly]
    If Not IsNothing(Format) Then Me.Format = Format
    If Not IsNothing(Password) Then Me.Password = Password
    If Not IsNothing(WriteResPassword) Then Me.WriteResPassword = WriteResPassword
    If Not IsNothing(IgnoreReadOnlyRecommended) Then Me.IgnoreReadOnlyRecommended = IgnoreReadOnlyRecommended
    If Not IsNothing(Origin) Then Me.Origin = Origin
    If Not IsNothing(Delimiter) Then Me.Delimiter = Delimiter
    If Not IsNothing(Editable) Then Me.Editable = Editable
    If Not IsNothing(Notify) Then Me.Notify = Notify
    If Not IsNothing(Converter) Then Me.Converter = Converter
    If Not IsNothing(AddToMru) Then Me.AddToMru = AddToMru
    If Not IsNothing(Local) Then Me.Local = Local
    If Not IsNothing(CorruptLoad) Then Me.CorruptLoad = CorruptLoad
    If Not IsNothing(Visible) Then Me.Visible = Visible
  End Sub

  Public Sub OpenExcel(Optional Filepath = Nothing, Optional Filename = Nothing, Optional NewApp = Nothing,
                           Optional UpdateLinks = Nothing, Optional [ReadOnly] = Nothing, Optional Format = Nothing,
                           Optional Password = Nothing, Optional WriteResPassword = Nothing, Optional IgnoreReadOnlyRecommended = Nothing,
                           Optional Origin = Nothing, Optional Delimiter = Nothing, Optional Editable = Nothing,
                           Optional Notify = Nothing, Optional Converter = Nothing, Optional AddToMru = Nothing,
                           Optional Local = Nothing, Optional CorruptLoad = Nothing, Optional Visible = Nothing)
    'Reset to default
    'Quit()

    oWB = Nothing
    If Not IsNothing(Filepath) Then Me.FilePath = Filepath
    If Not IsNothing(Filepath) Then Me.FileName = Path.GetFileName(Me.FilePath)
    If Not IsNothing(NewApp) Then Me.NewApp = NewApp

    If (Me.NewApp) Then                                                 'Checks if User wants to open a new Excel Application along with opening a Workbook
      OpenWorkbook()                                                  'Create new Excel Application
    Else
      Try
        oXl = Marshal.GetActiveObject("Excel.Application")          'Find an already existing Excel Application (if any)

        For Each IndivWorkbook As Excel.Workbook In oXl.Workbooks   'Checks if Workbook that User wants to open is already opened since Excel App is already running
          If (IndivWorkbook.FullName.Contains(Me.FileName)) Then
            OpenWorkbook(IndivWorkbook)                         'Set reference to an already opened Workbook
          End If
        Next IndivWorkbook

        If (oWB Is Nothing) Then
          OpenWorkbook()                                          'If Workbook is not found, try to open Workbook anyways
        End If
      Catch
        'No already-running Excel Application found, so create a new Application anyways
        OpenWorkbook()                                              'Then open Workbook
      End Try
    End If
  End Sub

  Public Sub OpenWorkbook(Optional oWB As Workbook = Nothing, Optional NewApp As Boolean = Nothing)
    'Open Excel with parameters user has inputted
    If Not (IsNothing(NewApp)) Then Me.NewApp = NewApp
    If IsNothing(oXl) Then oXl = New Excel.Application()
    oXl.DisplayAlerts = DisplayAlerts
    oXl.Visible = XlVisible

    'Open Workbook with parameters user has inputted
    If Not IsNothing(oWB) Then              'Workbook is already found open, set reference to that Workbook
      Me.oWB = oWB
    ElseIf (IsNothing(Me.FileName)) Then    'Create new Workbook
      Me.oWB = oXl.Workbooks.Add()
    Else                                    'Open existing Workbook
      Me.oWB = oXl.Workbooks.Open(FilePath, UpdateLinks, [ReadOnly], Format, Password, WriteResPassword, IgnoreReadOnlyRecommended,
          Origin, Delimiter, Editable, Notify, Converter, AddToMru, Local, CorruptLoad)
    End If

    oSheet = Me.oWB.ActiveSheet
  End Sub

  Public Function GetExcelPath() As String
    Return Me.FilePath
  End Function

  Public Sub Save(Optional FilePath As String = Nothing)
    Try
      If (IsNothing(FilePath)) Then FilePath = Me.FilePath
      oWB.SaveAs(FilePath)
    Catch ex As Exception
      MsgBox("Something went wrong. Couldn't save.")
    End Try
  End Sub

  Public Sub SaveAs()
    Dim SaveFileDialog As New Microsoft.Win32.SaveFileDialog()
    SaveFileDialog.RestoreDirectory = True
    SaveFileDialog.Filter = "Excel Worksheets|*.xls*"
    SaveFileDialog.ShowDialog()

    If (SaveFileDialog.FileName = "") Then Exit Sub

    Try
      oWB.SaveAs(SaveFileDialog.FileName)
    Catch ex As Exception
      MsgBox("Something went wrong. Couldn't save.")
    End Try
  End Sub

  Public Sub Quit()
    Try
      If Not (IsNothing(oSheet)) Then
        System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet)
        oSheet = Nothing
      End If

      If Not (IsNothing(oWB)) Then
        oWB.Close()
        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB)
        oWB = Nothing
      End If

      oXl.Quit()
      If Not (IsNothing(oXl)) Then
        System.Runtime.InteropServices.Marshal.ReleaseComObject(oXl)
        oXl = Nothing
      End If
    Catch ex As Exception
    Finally
      GC.Collect()
    End Try
  End Sub

  Public Sub SetSheet(Sheet As String)
    Try
      oSheet = oWB.Sheets(Sheet)
    Catch ex As Exception
    End Try
  End Sub

  Public Sub Write(Range As String, Data As String(,))
TryAgain:
    Try
      oRng = oSheet.Range(Range)
      oRng.Value = Data
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Sub

  Public Function Read(Range As String)
TryAgain:
    Try
      oRng = oSheet.Range(Range)
      Return oRng.Value
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function



  Public Function ReadArray(Range As String) As String(,)
TryAgain:
    Try
      Dim DataRaw As Object(,) = oSheet.Range(Range).Value
      Dim DataString As String(,) = Nothing
      Dim Row As Int16 = 0
      Dim Cell As Int16 = 0

      For Each xRow As Object() In DataRaw
        ReDim Preserve xRow(Row)
        For Each xCell As Object In xRow
          ReDim Preserve xCell(Cell)
          DataString(Row, Cell) = xCell
          Cell += 1
        Next xCell
        Row += 1
      Next xRow

      Return DataString
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function

  Public Function ReadValue(Range As String, Sheet As String) As String
    Dim Value As String
    If (IsNothing(Sheet)) Then
      Value = oSheet.Range(Range).Value
    Else
      Value = oWB.Sheets(Sheet).Range(Range).Value
    End If

    If (IsNothing(Value)) Then Return Nothing
    If (Value.Length = 0) Then Return Nothing
    Return Value
  End Function

  Public Function GetLastColumn(Row As String, Optional oSheet As String = Nothing) As String
TryAgain:
    Try
      If Not (IsNothing(oSheet)) Then SetSheet(oSheet)

      Return Me.oSheet.Cells(Row, Me.oSheet.Columns.Count).End(Excel.XlDirection.xlToLeft).Column
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function

  Public Function GetLastRow(Column As String, Optional oSheet As String = Nothing) As String
TryAgain:
    Try
      If Not (IsNothing(oSheet)) Then SetSheet(oSheet)

      Dim Row = Me.oSheet.Cells(Me.oSheet.Rows.Count, Column).End(Excel.XlDirection.xlUp).row
      Return Column & Row
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function

  Public Function GetOffset(Range As String, Optional Row As Int16 = 0, Optional Column As Int16 = 0)
TryAgain:
    Try
      Return oSheet.Range(Range).Offset(Row, Column).Address
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function

  Public Function GetRange(Range As String, Optional RowX As Int16 = 0, Optional ColumnX As Int16 = 0, Optional RowY As Int16 = 0, Optional ColumnY As Int16 = 0)
TryAgain:
    Try
      Return oSheet.Range(Range).Offset(RowX, ColumnX).Address & ":" & oSheet.Range(Range).Offset(RowY, ColumnY).Address
    Catch ex As Exception
      GoTo TryAgain
    End Try
  End Function


  Public Sub CreateXYGraph(XRange As String, YRange As String, Left As Double, Top As Double, Width As Double, Height As Double)
    oWB.Activate()

    Dim xlCharts As Excel.ChartObjects = oSheet.ChartObjects
    Dim myChart As Excel.ChartObject = xlCharts.Add(Left, Top, Width, Height)
    Dim Chart As Excel.Chart = myChart.Chart
    Dim Series As Excel.Series

    Chart.HasTitle = True
    Chart.ChartTitle.Text = "TPM Graph"
    Chart.HasLegend = False
    Chart.ChartType = Excel.XlChartType.xlXYScatterSmoothNoMarkers

    Chart.SeriesCollection.NewSeries()
    Series = CType(Chart.SeriesCollection(1), Excel.Series)

    Series.XValues = oSheet.Range(XRange)
    Series.Values = oSheet.Range(YRange)
  End Sub
End Class